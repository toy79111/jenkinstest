//
//  AppDelegate.h
//  JenkinsTest
//
//  Created by David Wang on 16/11/2016.
//  Copyright © 2016 David Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

